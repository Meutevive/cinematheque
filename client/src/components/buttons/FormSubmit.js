export const FormSubmit = ()=>{
    return(
        <button type="submit"
                className="text-black bg-white  font-medium rounded-lg text-sm w-32 px-6 py-2.5 text-center">
                Valider
        </button>
    );
}