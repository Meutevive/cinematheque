package com.cinamatheque.cinamatheque;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CinamathequeApplication {

	public static void main(String[] args) {
		SpringApplication.run(CinamathequeApplication.class, args);
	}

}
